.. role:: raw-html-m2r(raw)
   :format: html

DENSe
=====

Bayesian Density Estimation for Poisson Data

**DENSe** project homepage:
`http://ift.pages.mpcdf.de/public/dense <http://ift.pages.mpcdf.de/public/dense>`_

Description
-----------

**DENSe** is a compact library to enable Bayesian non-parametric inferences of densities of Poisson data counts.
Its framework of stateless methods is written in Python, although it relies on Numerical Information Field Theory (`NIFTy <https://gitlab.mpcdf.mpg.de/ift/nifty.git>`_) for the heavy lifting.
**DENSe** aims at utilizing all the available information in the data by modeling the inherent correlation structure using a Matérn kernel.
The inference of the density from count data can be written in a single line of python code.
The fitting method takes a multidimensional numpy array as input and returns multidimensional arrays of the same dimensions encoding the density field.

Setup
-----

Requirements
^^^^^^^^^^^^

* `Python 3 <(https://www.python.org/>`_ (3.6.x or later)
* `NIFTy <https://gitlab.mpcdf.mpg.de/ift/nifty.git>`_ (7.x)

Sources
^^^^^^^

The current version of DENSe can be obtained by cloning the repository:

.. code-block::

    git clone https://gitlab.mpcdf.mpg.de/ift/public/dense.git

Installation
^^^^^^^^^^^^

In the following, we assume a Debian-based distribution. For other distributions, the `apt` lines will need slight changes.

DENSe and its mandatory dependencies can be installed via:

.. code-block::

    sudo apt-get install git python3 python3-pip python3-dev
    pip3 install --user git+https://gitlab.mpcdf.mpg.de/ift/public/dense.git

Plotting support is added via:

.. code-block::

    sudo apt-get install python3-matplotlib

First Steps
-----------

For a quick start, dive into DENSe by running the example:

.. code-block::

    python3 demo/synthetic_example.py

Building the documentation from source
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To build the documentation from source, install `sphinx
<https://www.sphinx-doc.org/en/stable/index.html>`_\ and the `PyData Sphinx
Theme <https://github.com/pydata/pydata-sphinx-theme>`_ on your system and

.. code-block::

   sh doc/generate.sh

Acknowledgments
---------------

Please acknowledge the use of **DENSe** in your publication(s) by citing Guardiani et al. (2021) (`ADS entry <https://ui.adsabs.harvard.edu/abs/2021arXiv210513483G/abstract>`_).

Licensing terms
---------------

The DENSe package is licensed under the terms of the `GPLv3 <https://www.gnu.org/licenses/gpl.html>`_ and is distributed *without any warranty*.
