#!/usr/bin/env python3

import dense1 as de
import numpy as np
import matplotlib.pyplot as plt

model = de.build_density_estimator((128, 128))
# Load some data or generate it synthetically
data, synth_truth = de.draw_synthetic_sample(model, seed=60, pretty=True)

# Fit the model to the data and retrieve samples of the approximate posterior
# of the density
samples, _ = de.fit(model, data, n_max_iterations=5)

post_density_mean = np.mean(samples, axis=0)
post_density_std = np.std(samples, axis=0)

im_kw = {"vmin": synth_truth.min(), "vmax": synth_truth.max()}
fig, axs = plt.subplots(2, 2, figsize=(16, 10))
im = axs.flat[0].imshow(synth_truth, **im_kw)
axs.flat[0].set_title("Synthetic Truth")
im = axs.flat[1].imshow(data, **im_kw)
axs.flat[1].set_title("Data")
im = axs.flat[2].imshow(post_density_mean, **im_kw)
axs.flat[2].set_title("Posterior Mean")
im = axs.flat[3].imshow(post_density_std, **im_kw)
axs.flat[3].set_title("Posterior Std")
fig.tight_layout()
fig.colorbar(im, ax=axs.ravel().tolist(), orientation='vertical', shrink=0.75)
fig.savefig("synthetic_example.png")
plt.close()
