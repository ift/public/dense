#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="dense1",
    version="0.0.1",
    author="Gordian Edenhofer, Torsten Enßlin, Philipp Frank, Matteo Guardiani, Jakob Roth",
    author_email="edh@mpa-garching.mpg.de",
    url="https://gitlab.mpcdf.mpg.de/ift/public/dense",
    description="Bayesian Density Estimation for Poisson Data",
    packages=find_packages(include=["dense1", "dense1.*"]),
    license="GPLv3",
    setup_requires=["numpy>=1.17", "nifty7>=7.0"],
    install_requires=["numpy>=1.17", "nifty7>=7.0"],
    # tests_require=["pytest", "pytest-cov"],
    python_requires=">=3.7",
    zip_safe=True,
    classifiers=[
        "Development Status :: 4 - Beta", "Topic :: Utilities",
        "License :: OSI Approved :: GNU General Public License v3 "
        "or later (GPLv3+)"
    ],
)
